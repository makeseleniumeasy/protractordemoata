exports.config = {

    /*
    If you want to launch chrome browser with custom capabilities, you need to use
    'capabilities' argument. Chrome options are nested in the chromeOptions object. 
    You can find all capabilities here: 
    https://peter.sh/experiments/chromium-command-line-switches/
    */
   capabilities: {
  'browserName': 'chrome',
  'chromeOptions': {
    'args': ['--start-maximized', '--disable-notifications' ]
  }
}, 
    specs: ['../Spec Files/LaunchAngularSite.js']
}