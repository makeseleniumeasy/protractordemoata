/*
Protractor does not downalod IE driver by default. It downloads only
for Firefox and chrome. So you must need to download through below command: 
'webdriver-manager update --ie'
*/
exports.config = {

    capabilities: {
        'browserName': 'internet explorer',
        'args': ['--start-maximized']
    },
    specs: ['../Spec Files/LaunchAngularSite.js']
}