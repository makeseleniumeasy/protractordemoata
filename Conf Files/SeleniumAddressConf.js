/*
Protractor by default starts selenium server and run tests on that server. If we want to run tests
on a specific server, we need to use 'seleniumAddress' and pass address. We can start selenium server
at desired port and can pass here.
*/
exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['../Spec Files/LaunchAngularSite.js'],
    capabilities: {
        browserName: 'chrome'
    }
}