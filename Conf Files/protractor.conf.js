/*
When we run command as 'protractor', it will lokk for the default protractor configuration file
with name "protractor.conf.js". "Chrome" is default browser and it will be launched with default
capabilities. It will not be maximized. 
*/
exports.config = {

    /* Pass this argument with spec(test file) name to run tests. You can pass multiple spec file names using
    comma. 
    */
    specs: ['../Spec Files/LocatingElements.js']
}