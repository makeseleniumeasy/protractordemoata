var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');
exports.config = {


  capabilities: {
    'browserName': 'chrome',
    'chromeOptions': {
      'args': ['--start-maximized', '--disable-notifications'],
    },
    
    specs: ['LocatingElementsOptimal.js', 'LocatingListOfElements.js']
  },

  onPrepare: function () {
    jasmine.getEnv().addReporter(
      new Jasmine2HtmlReporter({
        savePath: 'target/screenshots'
      })
    );
  }
}