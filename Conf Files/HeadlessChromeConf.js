exports.config={
    capabilities: {
        browserName: 'chrome',

        chromeOptions: {
            args: ["--headless", "--disable-gpu", "--window-size=800,600"]
        }
    },
    specs: ['../Spec Files/LaunchAngularSite.js']

}