/*
If you do not want protractor to start the selenium serve, use diretConnect as true.
*/

exports.config = {
    directConnect: true,
    
    capabilities: {
        'browserName': 'chrome'
    },
    specs: ['../Spec Files/LaunchAngularSite.js']

}