// conf.js
// var HTTPSProxyAgent = require('https-proxy-agent');
// var sauceRestAgent = new HTTPSProxyAgent("http://<proxy>:<port>")

exports.config = {
    sauceUser: 'KiranKabadiLive',
    sauceKey: 'a98ade00-1bd4-4943-bbe4-3d34250eb630',
    // parentTunnel: 'KiranKabadiLive',
    tunnelIdentifier: 'myTunnel',

    specs: ['../Spec Files/LaunchAngularSite.js'],

    // onPrepare: function () {
    //     var caps = browser.getCapabilities()
    // },

    multiCapabilities: [{
        browserName: 'firefox',
        version: 'latest',
        platform: 'OS X 10.10',
        name: "firefox-tests",
        shardTestFiles: true,
        maxInstances: 25
    }, {
        browserName: 'internet explorer',
        version: '11',
        platform: 'Windows 10',
        name: "ie-tests",
        shardTestFiles: true,
        maxInstances: 25
    },{
        browserName: 'chrome',
        version: '41',
        platform: 'Windows 7',
        name: "chrome-tests",
        shardTestFiles: true,
        maxInstances: 25
    }],

}
