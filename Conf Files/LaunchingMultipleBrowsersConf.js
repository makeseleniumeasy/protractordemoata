/*
If you want to use multiple different browsers, you need to use 'multiCapabilities'.
*/
exports.config={
    multiCapabilities: [{
  'browserName': 'firefox'
}, {
  'browserName': 'chrome'
}],
  specs: ['../Spec Files/LaunchAngularSite.js']
}