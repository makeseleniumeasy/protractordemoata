exports.config = {

    /*
    If you want to launch firefox browser with custom capabilities, you need to use
    'capabilities' argument. Firefox options are nested in the moz:firefoxOptions object.
    */
    capabilities: {
        'browserName': 'firefox',
        'moz:firefoxOptions': {
            'args': ['--safe-mode'] // Firefox is default launched in maximized window.
        }
    },
   specs: ['../Spec Files/LaunchAngularSite.js'] 
}