exports.config = {
   capabilities: {
      browserName: 'firefox',

      'moz:firefoxOptions': {
         args: ["--headless"]
      }
   },
   specs: ['../Spec Files/LaunchAngularSite.js']

}