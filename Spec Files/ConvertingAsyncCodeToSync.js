/*
Making Async code to sync using a function with callback.
*/
describe('Protractor Demo App', function () {
    function startExecution(callback)
    {
        console.log("Execution started.")
        callback();
    }

    function endExecution(callback)
    {
        console.log("Execution ends.")
        callback();

    }
    
    it('should have a title', function (callback) {
        startExecution(function(){
            browser.get('http://juliemr.github.io/protractor-demo/'); // Protractor Code. Will follow control flow.
            expect(browser.getTitle()).toEqual('Super Calculator').then(function(){
                endExecution(function(){
                    callback();
                })
            }); 
        })
        
        
    });
});
