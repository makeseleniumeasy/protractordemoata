describe('Protractor Demo App', function () {

    var firstOperand = element(by.model('first'));
    var secondOperand = element(by.model('second'));
    var btnGo = element(by.id('gobutton'));
    var history = element.all(by.xpath("//table[@class='table']//tr//td[3]"));

    function add(a, b) {
        firstOperand.sendKeys(a);
        secondOperand.sendKeys(b);
        btnGo.click();
    }


    beforeEach(function () {
        browser.get('http://juliemr.github.io/protractor-demo/');
    });

    it('should have a history', function () {
        add(1, 2);
        add(3, 4);
        add(5, 6);

        // Assert count of results
        expect(history.count()).toEqual(3);

        // Print each result value
        history.each(function (result) {
            result.getText().then(function (val) {
                console.log(val)
            })
        })

        // Directly printing first and last
        history.first().getText().then(function (val) {
            console.log(val)
        })

        history.last().getText().then(function (val) {
            console.log(val)
        })

        // Printing value of particlar index
        history.get(1).getText().then(function (val) {
            console.log(val)
        })
    });
});