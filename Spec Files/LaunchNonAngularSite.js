describe('Launching a non-angular website: ', function () {
    it('should have a title', function () {

        browser.waitForAngularEnabled(false);
        browser.get('https://www.google.com/');
        expect(browser.getTitle()).toEqual('Google');

    });
});