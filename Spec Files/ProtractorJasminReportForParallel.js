var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');
var today = new Date();
var timeStamp = today.getMonth() + 1 + '-' + today.getDate() + '-' + today.getFullYear() + '-' + today.getHours() + 'h-' + today.getMinutes() + 'm-' + today.getSeconds() + 's';

exports.config = {

  capabilities: {
    'browserName': 'chrome',
    maxInstances: 2,
    'shardTestFiles': true,
    'chromeOptions': {
      'args': ['--start-maximized', '--disable-notifications'],
    },
    specs: ['LocatingElementsOptimal.js', 'LocatingListOfElements.js']
  },

  onPrepare: function () {
    jasmine.getEnv().addReporter(
      new Jasmine2HtmlReporter({
        savePath: 'target/screenshots' + timeStamp,
      })
    );
  }
}