/*
This example shows asynchornous behaviour of Protractor as it is built as a wrapper on
WebDriverJS API (Selenium Javascript binding) and it is Asynchornous. WebDriverJS is async because JavaScript is. All functions return promises. 
WebDriverJS maintains a queue of pending promises, called the control flow, to keep execution organized.
*/
/*
Protractor adapts Jasmine so that each spec automatically waits until the control flow is empty before exiting.
Jasmine expectations are also adapted to understand promises. 
The 'expect' code actually adds an expectation task to the control flow, which will run after the other tasks: 
*/
describe('Protractor Demo App', function () {
    it('should have a title', function () {
        console.log("Execution started.")     // Non-Protractor code. Will run Asynchronus.
        browser.get('http://juliemr.github.io/protractor-demo/'); // Protractor Code. Will follow control flow.

        expect(browser.getTitle()).toEqual('Super Calculator'); // Protractor Code. Will follow control flow.
        console.log("Execution ends.")  // Non-Protractor code. Will run Asynchronus.
    });
}); 