/*
This program shows optimal way of writing protractor tests. It also helps in automatic handling of 
StaleElementException as 'element' gloabl object looks for an element when it is used everytime which 
is the same as PageFactory in Selenium WebDriver.

You can see after refreshing page, code works fine with stored reference.
*/

describe('Protractor Demo App', function () {
  it('should add one and two', function () {
    browser.get('http://juliemr.github.io/protractor-demo/');
    var firstOperand = element(by.model('first'));
    var secondOperand = element(by.model('second'));
    var btnGo = element(by.id('gobutton'));
    var lblResult = element(by.xpath("//h2[@class='ng-binding']"))
    
    // Positive Test case 
    firstOperand.sendKeys(1);
    secondOperand.sendKeys(2);
    btnGo.click();
    expect(lblResult.getText()).toEqual('3');

    // Refresh page
    browser.refresh();

    // Negative Test case 
    firstOperand.sendKeys(2);
    secondOperand.sendKeys(5);
    btnGo.click();
    expect(lblResult.getText()).toEqual('7');
  });
});