describe('Protractor Demo App', function() {
  it('should add one and two', function() {
    browser.get('http://juliemr.github.io/protractor-demo/');
    browser.manage().window().maximize();

    // Positive Test case 
    element(by.model('first')).sendKeys(1);
    element(by.model('second')).sendKeys(2);

    element(by.id('gobutton')).click();

    expect(element(by.xpath("//h2[@class='ng-binding']")).getText()).toEqual('3'); 


    // Negative Test case 
    element(by.model('first')).sendKeys(2);
    element(by.model('second')).sendKeys(5);

    element(by.id('gobutton')).click();

    expect(element(by.xpath("//h2[@class='ng-binding']")).getText()).toEqual('7');
  });
});