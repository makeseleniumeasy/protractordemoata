exports.config={


// Configuration to run spec files in same browser parallely 
   /* capabilities: {
        'browserName': 'chrome',
        'shardTestFiles': true,
        'chromeOptions': {
            'args': ['--start-maximized', '--disable-notifications'],
        },
        maxInstances: 2,
        specs: ['LocatingElementsOptimal.js', 'LocatingListOfElements.js']
    } */


    // Configuration to run spec files in different browsers parallely
    multiCapabilities: [
        {
            "browserName": "chrome",
            shardTestFiles: true,
            maxInstances: 2,
            specs: ['LocatingElementsOptimal.js', 'LocatingListOfElements.js']
        },
        {
            "browserName": "firefox",
            shardTestFiles: true,
            "count": 2,
            specs: ['LocatingElementsOptimal.js', 'LocatingListOfElements.js']
        },
    ],
}